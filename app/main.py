import json 
import time

#third-party imports
from faker import Faker
from fastapi import FastAPI
from kafka import KafkaProducer

def json_serializer(data):
    '''
    serializer to serializer data before publishing it to kafka topic
    '''
    return json.dumps(data).encode('utf-8')
    
#creating a producer instance
producer = KafkaProducer(
                        bootstrap_servers = ['13.233.136.253:9092'],
                        value_serializer=json_serializer,
                        # partitioner=get_partition
                        )

app = FastAPI()

@app.get('/endpoint')
def show():
    registered_user = get_registered_user()
    print(registered_user)
    producer.send("registered_user",registered_user)
    return "Message added to the kafka is {}".format(registered_user)

#creating a faker instance
fake = Faker()

def get_registered_user():
    '''
    this method will return a fake registered user
    '''
    return {
        "name":fake.name(),
        "address":fake.address(),
        "created_at":fake.year()
    }



    