

FROM tiangolo/uvicorn-gunicorn-fastapi:python3.7
 
COPY ./requirements.txt /app/requirements.txt
 
RUN pip install -r /app/requirements.txt
 
COPY ./app /app



# CMD ["uvicorn", "app.main:app", "--host", "0.0.0.0", "--port", "80"]